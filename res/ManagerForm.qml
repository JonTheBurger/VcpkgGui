import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

Page {
    id: homeForm
    width: parent ? parent.width : 640
    height: parent ? parent.height : 480
    padding: 10
    title: qsTr("Manage Packages")

    ColumnLayout {
        anchors.fill: parent
        RowLayout {
            CheckBox {
                onCheckStateChanged: {
                    backend.useInstallFilter(checked)
                }
            }

            TextField {
                placeholderText: qsTr("Filter...")
                onTextChanged: {
                    backend.setPackageFilter(text)
                }

                font.pixelSize: 18
                selectByMouse: true
                Layout.fillWidth: true
            }
        }

        ListView {
            id: listView
            model: backend.model
            clip: true
            Layout.fillWidth: true
            Layout.fillHeight: true
            ScrollBar.vertical: ScrollBar {
                active: true
            }
            header: RowLayout {
                anchors.left: parent.left
                anchors.right: parent.right
                Text {
                    Layout.fillWidth: true
                    text: qsTr("Package")
                    font.bold: true
                }
                Text {
                    Layout.alignment: Qt.AlignRight
                    Layout.rightMargin: 20
                    text: qsTr("Version")
                    font.bold: true
                }
            }

            BusyIndicator {
                running: backend.isLoading
                anchors.fill: parent
            }

            highlight: Item {
                Rectangle {
                    color: "violet"
                    radius: 3
                    width: parent.width - 10
                    height: parent.height
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                }
            }
            highlightFollowsCurrentItem: true
            highlightMoveDuration: 10
            focus: true

            delegate: RowLayout {
                anchors.left: parent.left
                anchors.right: parent.right

                CheckBox {
                    id: installCheckBox
                    //                    onClicked: {
                    //                        backend.model.isInstalled = checked
                    //                        console.log(backend.model.isInstalled)
                    //                    }
                    //                    Component.onCompleted: checked = backend.model.isInstalled
                    //                    Connections {
                    //                        target: backend.model
                    //                        onIsInstalledChanged: installCheckBox.checked = backend.model.isInstalled
                    //                    }

                    //                    Binding {
                    //                        target: backend.model
                    //                        property: "isInstalled"
                    //                        value: installCheckBox.checked
                    //                    }
                    checked: isInstalled
                    onClicked: {
                        backend.model.isInstalled = checked // BUG: doesn't propagate to C++
                        backend.updateDirty(model.name, checked)
                        console.log(checked)
                    }
                }
                Text {
                    Layout.fillWidth: true
                    text: model.name
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            listView.currentIndex = index
                            descriptionLabel.text = model.description
                        }
                    }
                }
                Text {
                    Layout.alignment: Qt.AlignRight
                    Layout.rightMargin: 20
                    text: model.version
                }
            }
        }

        Text {
            id: descriptionLabel
            text: qsTr("Select package for description...")
            Layout.fillWidth: true
        }
    }
}
