import QtQuick 2.0
import QtQml 2.0

QtObject {
    property var model:
    ListModel {
        ListElement {
            name: "PackageName"
            version: "1.0.0"
            description: "An example package"
            installed: false
        }
        ListElement {
            name: "PackageName"
            version: "1.0.0"
            description: "An example package"
            installed: false
        }
        ListElement {
            name: "PackageName"
            version: "1.0.0"
            description: "An example package"
            installed: false
        }
    }
}

