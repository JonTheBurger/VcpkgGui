import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

Page {
    width: parent ? parent.width : 640
    height: parent ? parent.height : 480
    padding: 10
    title: qsTr("Setup")

    ColumnLayout {
        width: parent.width

        TextField {
            id: pathEntry
            Layout.fillWidth: parent
            font.pixelSize: 18
            selectByMouse: true
            placeholderText: qsTr("vcpkg root")
            text: backend.vcpkgPath
            Binding {
                target: backend
                property: "vcpkgPath"
                value: pathEntry.text
            }
        }

        RowLayout {
            Text {
                text: qsTr("Triplet:")
                font.pixelSize: 18
            }

            ComboBox {
                Layout.fillWidth: parent
                textRole: "display"
                model: backend.supportedTriplets
                currentIndex: find(backend.triplet);
                Component.onCompleted: {
                    currentIndex = find(backend.triplet);
                }
                onCurrentTextChanged: {
                    if (backend.triplet != currentText) {
                        backend.triplet = currentText
                        backend.loadPackages()
                    }
                }
            }
        }
    }
}
