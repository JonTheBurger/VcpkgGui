import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

Page {
    width: parent ? parent.width : 640
    height: parent ? parent.height : 480
    padding: 10
    title: qsTr("Help")

    ColumnLayout {
        width: parent.width
        Text {
            font.pixelSize: 18
            text: qsTr("<a href='https://vcpkg.readthedocs.io/en/latest/'>vcpkg help</a>")
            onLinkActivated: Qt.openUrlExternally("https://vcpkg.readthedocs.io/en/latest/")
        }
        RowLayout {
            Text {
                font.pixelSize: 18
                Layout.fillWidth: parent
                text: qsTr("GUI version:")
            }
            Text {
                font.pixelSize: 18
                Layout.alignment: Qt.AlignRight
                text: qsTr("0.0.1")
            }
        }
        RowLayout {
            Text {
                font.pixelSize: 18
                Layout.fillWidth: parent
                text: qsTr("vcpkg version:")
            }
            Text {
                font.pixelSize: 18
                Layout.alignment: Qt.AlignRight
                text: qsTr("abcde")
            }
        }
    }
}
