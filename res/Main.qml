import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3

ApplicationWindow {
    id: window
    visible: true
    width: 640
    height: 480
    title: qsTr("vcpkg gui")

    header: ToolBar {
        RowLayout {
            anchors.fill: parent

            ToolButton {
                text: stackView.depth > 1 ? "◀" : "☰"
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                onClicked: {
                    if (stackView.depth > 1) {
                        stackView.pop()
                    } else {
                        drawer.open()
                    }
                }
            }

            Label {
                Layout.fillWidth: true
                text: stackView.currentItem.title
            }

            ToolButton {
                id: browseButton
                text: "🔍"
                onClicked: {
                    fileDialog.open()
                }
                Binding {
                    target: backend
                    property: "vcpkgPath"
                    value: fileDialog.folder
                }
                FileDialog {
                    id: fileDialog
                    title: qsTr("vcpkg location...")
                    selectFolder: true
                    folder: backend.vcpkgPath
                }
            }

            ToolButton {
                id: syncButton
                text: "⇵" // "⤓" // "↻"
                onClicked: {
                    backend.syncPackages()
                }
            }
        }
    }

    Drawer {
        id: drawer
        width: window.width * 0.66
        height: window.height

        Column {
            anchors.fill: parent

            ItemDelegate {
                text: qsTr("Setup")
                width: parent.width
                onClicked: {
                    stackView.push("SetupForm.qml")
                    drawer.close()
                }
            }
            ItemDelegate {
                text: qsTr("Help")
                width: parent.width
                onClicked: {
                    stackView.push("HelpForm.qml")
                    drawer.close()
                }
            }
        }
    }

    StackView {
        id: stackView
        initialItem: "ManagerForm.qml"
        anchors.fill: parent
    }
}
