#include <QMetaProperty>

#include "QObjectListModel.hxx"

QObjectListModel::QObjectListModel(std::unique_ptr<const QObject> prototype, QObject* const parent)
    : QAbstractListModel(parent)
    , items_()
    , prototype_(std::move(prototype))
    , roles_() {
  Q_ASSERT(prototype_);
  auto meta          = prototype_->metaObject();
  auto dynamicRoleId = Qt::UserRole + 1;

  for (auto i = meta->propertyOffset(); i < meta->propertyCount(); ++i) {
    roles_.insert(dynamicRoleId, meta->property(i).name());
    ++dynamicRoleId;
  }
}

int QObjectListModel::rowCount(const QModelIndex& parent) const {
  (void)parent;
  Q_ASSERT(items_.size() <= static_cast<decltype(items_.size())>(~0) >> 1);
  return static_cast<int>(items_.size());
}

QVariant QObjectListModel::data(const QModelIndex& index, const int role) const {
  if (!index.isValid() || index.row() >= rowCount()) { return QVariant(); }

  auto* qobj          = items_[static_cast<size_t>(index.row())].get();
  auto  meta          = qobj->metaObject();
  auto  dynamicRoleId = Qt::UserRole + 1;
  for (auto i = meta->propertyOffset(); i < meta->propertyCount(); ++i) {
    if (role == dynamicRoleId) {
      return meta->property(i).read(qobj);
    }
    ++dynamicRoleId;
  }

  return QVariant();
}

QHash<int, QByteArray> QObjectListModel::roleNames() const {
  return roles_;
}

const QObject& QObjectListModel::operator[](int idx) const {
  Q_ASSERT(idx >= 0);
  return *items_[static_cast<size_t>(idx)];
}

QObject& QObjectListModel::operator[](int idx) {
  return const_cast<QObject&>(static_cast<const QObjectListModel&>(*this)[idx]);
}

void QObjectListModel::clear() {
  if (!items_.empty()) {
    beginRemoveRows(QModelIndex(), 0, rowCount() - 1);
    items_.clear();
    endRemoveRows();
  }
}

void QObjectListModel::append(std::unique_ptr<QObject> obj) {
  Q_ASSERT(obj);
  Q_ASSERT(prototype_->metaObject()->className() == obj->metaObject()->className());

  beginInsertRows(QModelIndex(), rowCount(), rowCount());
  items_.push_back(std::move(obj));
  endInsertRows();
}
