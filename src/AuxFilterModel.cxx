#include <QtGlobal>

#include "AuxFilterModel.hxx"

AuxFilterModel::AuxFilterModel(QObject* const parent)
    : QSortFilterProxyModel(parent)
    , auxFilterKey_()
    , auxFilterRole_(Qt::DisplayRole)
    , useAuxFilter_(false) {
}

QVariant AuxFilterModel::auxFilterKey() const {
  return auxFilterKey_;
}

void AuxFilterModel::setAuxFilterKey(QVariant filterKey) {
  auxFilterKey_ = std::move(filterKey);
  invalidateFilter();
  auxFilterKeyChanged(auxFilterKey_);
}

int AuxFilterModel::auxFilterRole() const {
  return auxFilterRole_;
}

void AuxFilterModel::setAuxFilterRole(int filterRole) {
  auxFilterRole_ = filterRole;
  invalidateFilter();
  auxFilterRoleChanged(filterRole);
}

bool AuxFilterModel::useAuxFilter() const {
  return useAuxFilter_;
}

void AuxFilterModel::setUseAuxFilter(bool useFilter) {
  useAuxFilter_ = useFilter;
  invalidateFilter();
  useAuxFilterChanged(useFilter);
}

bool AuxFilterModel::filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const {
  bool acceptAuxData = true;
  if (useAuxFilter_) {
    const QModelIndex idx = sourceModel()->index(sourceRow, 0, sourceParent);
    acceptAuxData         = sourceModel()->data(idx, auxFilterRole_) == auxFilterKey_;
  }

  return acceptAuxData && QSortFilterProxyModel::filterAcceptsRow(sourceRow, sourceParent);
}
