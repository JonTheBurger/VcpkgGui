﻿#include <QDebug>
#include <QtAlgorithms>

#include "Backend.hxx"
#include "PackageInfo.hxx"
#include "QObjectListModel.hxx"

/** <\brief String value representing the default triplet used by vcpkg. */
static const QString DEFAULT_TRIPLET =
#if defined(_WIN32)
    QStringLiteral("x86-windows");
#elif defined(__APPLE__)
    QStringLiteral("x64-osx");
#elif defined(__FreeBSD__)
    QStringLiteral("x64-freebsd");
#else
    QStringLiteral("x64-linux");
#endif

Backend::Backend()
    : QObject(nullptr)
    , modelData_(std::make_unique<QObjectListModel>(PackageInfo::Default()))
    , model_(std::make_unique<AuxFilterModel>())
    , supportedTriplets_(std::make_unique<QStringListModel>())
    , triplet_(DEFAULT_TRIPLET)
    , vcpkgPath_()
    , installedPackages_()
    , isLoading_(false) {
  auto roles           = modelData_->roleNames();
  auto packageNameRole = std::find_if(roles.constKeyValueBegin(), roles.constKeyValueEnd(), [](const std::pair<int, QByteArray>& pair) -> bool {
    return QString(pair.second) == PackageInfo::NameRoleStr;
  });
  auto installedRole   = std::find_if(roles.constKeyValueBegin(), roles.constKeyValueEnd(), [](const std::pair<int, QByteArray>& pair) -> bool {
    return QString(pair.second) == PackageInfo::IsInstalledRoleStr;
  });

  Q_ASSERT(packageNameRole != roles.constKeyValueEnd());
  Q_ASSERT(installedRole != roles.constKeyValueEnd());
  nameRole_        = (*packageNameRole).first;
  isInstalledRole_ = (*installedRole).first;
  model_->setSourceModel(modelData_.get());
  model_->setFilterRole(nameRole_);
  model_->setAuxFilterRole(isInstalledRole_);
  model_->setAuxFilterKey(true);
  model_->setFilterCaseSensitivity(Qt::CaseInsensitive);

  loadDefaultLocationAsync();
  loadTripletsAsync();
  loadPackages();
}

Backend::~Backend() = default;

void Backend::loadPackages() {
  setIsLoading(true);
  modelData_->clear();
  dirtyPackages_.clear();

  vcpkgListAsync();
}

void Backend::syncPackages() {
  setIsLoading(true);
  QStringList toInstall;
  QStringList toRemove;

  for (auto&& package : dirtyPackages_.keys()) {
    const bool isAdded = dirtyPackages_[package];
    if (isAdded) {
      toInstall.push_back(package + ':' + triplet_);
    } else {
      toRemove.push_back(package + ':' + triplet_);
    }
  }
  dirtyPackages_.clear();

  installAsync(toInstall);
  removeAsync(toRemove);
}

void Backend::setPackageFilter(const QString& filter) {
  model_->setFilterFixedString(filter);
}

void Backend::useInstallFilter(bool useFilter) {
  model_->setUseAuxFilter(useFilter);
}

void Backend::updateDirty(const QString& name, bool install) {
  if (dirtyPackages_.contains(name)) {
    dirtyPackages_.remove(name);
  } else {
    dirtyPackages_.insert(name, install);
  }
}

AuxFilterModel* Backend::model() {
  return model_.get();
}

QStringListModel* Backend::supportedTriplets() {
  return supportedTriplets_.get();
}

QString Backend::triplet() const {
  return triplet_;
}

void Backend::setTriplet(QString triplet) {
  if (triplet_ != triplet) {
    triplet_ = std::move(triplet);
    emit tripletChanged();
  }
}

QString Backend::vcpkgPath() const {
  return vcpkgPath_;
}

void Backend::setVcpkgPath(const QString& path) {
  if (vcpkgPath_ != path) {
    vcpkgPath_ = path.simplified();
    emit vcpkgPathChanged();
  }
}

QString Backend::vcpkgExe() const {
  return vcpkgPath().mid(sizeof("file://") - sizeof('\0'));
}

bool Backend::isLoading() const {
  return isLoading_;
}

void Backend::setIsLoading(bool isLoading) {
  if (isLoading_ != isLoading) {
    isLoading_ = isLoading;
    emit isLoadingChanged();
  }
}

void Backend::loadDefaultLocationAsync() {
  QStringList args{ QStringLiteral("vcpkg") };
  QProcess    p;
  p.start("which", args);
  p.waitForFinished();
  setVcpkgPath((QLatin1String("file://") + p.readAllStandardOutput()));
  runProcess("which", args, [this](const QString& out) { setVcpkgPath(QLatin1String("file://") + out); });
}

void Backend::loadTripletsAsync() {
  QStringList args{ QStringLiteral("help"), QStringLiteral("triplets") };
  runProcess(vcpkgExe(), args, [this](const QString& out) { parseVcpkgTriplets(out); });
}

void Backend::vcpkgSearchAsync() {
  QStringList args{ QStringLiteral("search") };
  runProcess(vcpkgExe(), args, [this](const QString& out) { parseVcpkgSearch(out); }, [this]() { setIsLoading(false); });
}

void Backend::vcpkgListAsync() {
  QStringList args{ QStringLiteral("list") };
  runProcess(vcpkgExe(), args, [this](const QString& out) { parseVcpkgList(out); }, [this]() { vcpkgSearchAsync(); });
}

void Backend::installAsync(const QStringList& newPackages) {
  if (newPackages.empty()) { return; }
  QStringList args{ QStringLiteral("install"), QStringLiteral("--keep-going") };
  args << newPackages;

  runProcess(vcpkgExe(), args, [this](const QString&) { loadPackages(); });
}

void Backend::removeAsync(const QStringList& doomedPackages) {
  if (doomedPackages.empty()) { return; }
  QStringList args{ QStringLiteral("remove") };
  args << doomedPackages;

  runProcess(vcpkgExe(), args, [this](const QString&) { loadPackages(); });
}

void Backend::parseVcpkgTriplets(const QString& vcpkgOutput) {
  auto lines = vcpkgOutput.split(QRegExp("\n|\r\n|\r"), QString::SkipEmptyParts);
  lines.removeFirst(); // ignore "Available architecture triplets:"

  for (auto i = 0; i < lines.size(); ++i) {
    if (supportedTriplets_->insertRow(supportedTriplets_->rowCount())) {
      QModelIndex index   = supportedTriplets_->index(supportedTriplets_->rowCount() - 1, 0);
      const auto  triplet = lines[i].trimmed();
      supportedTriplets_->setData(index, triplet);
    }
  }
}

void Backend::parseVcpkgList(const QString& vcpkgOutput) {
  QSet<QString> installedPackages;
  auto          lines = vcpkgOutput.split(QRegExp("\n|\r\n|\r"), QString::SkipEmptyParts);

  for (auto&& line : lines) {
    auto packageByTriple = line.split(' ').takeFirst();
    // packages are printed in the form packageName:triplet
    if (packageByTriple.contains(':' + triplet_)) {
      auto package = packageByTriple.split(':').takeFirst();
      installedPackages.insert(package);
    }
  }

  installedPackages_ = std::move(installedPackages);
}

void Backend::parseVcpkgSearch(const QString& vcpkgOutput) {
  std::vector<std::unique_ptr<QObject>> objects;
  auto                                  packages      = vcpkgOutput.split(QRegExp("\n|\r\n|\r"));
  QChar                                 previousAlpha = '\0';
  for (auto&& package : packages) {
    // Used to ignore some extra messages that can appear after the final package is listed.
    if (package[0] < previousAlpha) {
      break;
    }
    previousAlpha = package[0];

    auto    tokens      = package.split(' ', QString::SkipEmptyParts);
    QString name        = tokens.takeFirst();
    QString version     = tokens.takeFirst();
    QString description = tokens.join(' ');
    bool    installed   = installedPackages_.contains(name);

    objects.emplace_back(std::make_unique<PackageInfo>(std::move(name), std::move(version), std::move(description), installed, modelData_.get()));
  }
  modelData_->appendMany(std::move(objects));
}

void parseVcpkgInstall(const QString& vcpkgOutput) {
  (void)vcpkgOutput;
}
