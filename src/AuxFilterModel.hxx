#ifndef AUXFILTERMODEL_HXX
#define AUXFILTERMODEL_HXX

#include <QSortFilterProxyModel>

#include "DeclQmlProperty.hxx"

/** \brief Provides an additional level of filtering to a SortFilterProxyModel. */
class AuxFilterModel final : public QSortFilterProxyModel {
  Q_OBJECT
  Q_PROPERTY(QVariant auxFilterKey READ auxFilterKey WRITE setAuxFilterKey NOTIFY auxFilterKeyChanged)
  Q_PROPERTY(int auxFilterRole READ auxFilterRole WRITE setAuxFilterRole NOTIFY auxFilterRoleChanged)
  Q_PROPERTY(bool useAuxFilter READ useAuxFilter WRITE setUseAuxFilter NOTIFY useAuxFilterChanged)

public:
  AuxFilterModel(QObject* parent = nullptr);
  QVariant auxFilterKey() const;                /**< \brief Accessor method used for Q_PROPERTY. */
  void     setAuxFilterKey(QVariant filterKey); /**< \brief Mutator method used for Q_PROPERTY. */
  int      auxFilterRole() const;               /**< \brief Accessor method used for Q_PROPERTY. */
  void     setAuxFilterRole(int filterRole);    /**< \brief Mutator method used for Q_PROPERTY. */
  bool     useAuxFilter() const;                /**< \brief Accessor method used for Q_PROPERTY. */
  void     setUseAuxFilter(bool useFilter);     /**< \brief Mutator method used for Q_PROPERTY. */

signals:
  void auxFilterKeyChanged(const QVariant& filterKey); /**< \brief Notification method used for Q_PROPERTY. */
  void auxFilterRoleChanged(int filterRole);           /**< \brief Notification method used for Q_PROPERTY. */
  void useAuxFilterChanged(bool doUseFilter);          /**< \brief Notification method used for Q_PROPERTY. */

protected:
  bool filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const override;

private:
  QVariant auxFilterKey_;  /**< \brief Value used to compare against for filtering. */
  int      auxFilterRole_; /**< \brief Role of QObject used to compare agains. */
  bool     useAuxFilter_;  /**< \brief Toggle to turn filtering on/off. */
};

#endif // AUXFILTERMODEL_HXX
