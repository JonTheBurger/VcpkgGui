#ifndef QOBJECTLISTMODEL_HXX
#define QOBJECTLISTMODEL_HXX

#include <memory>
#include <vector>

#include <QAbstractListModel>

/** \brief Provides a read-only interface to supply QMetaObject properties to Views as roles. */
class QObjectListModel final : public QAbstractListModel {
  Q_OBJECT
  Q_DISABLE_COPY(QObjectListModel)

public:
  QObjectListModel(std::unique_ptr<const QObject> prototype, QObject* parent = nullptr);

  Q_INVOKABLE QVariant   data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
  int                    rowCount(const QModelIndex& parent = QModelIndex()) const override;
  QHash<int, QByteArray> roleNames() const override;
  const QObject&         operator[](int idx) const;
  QObject&               operator[](int idx);
  void                   clear();                              /**< \brief Deletes all data in the underlying storage, signaling pertinent notifications. */
  void                   append(std::unique_ptr<QObject> obj); /**< \brief Adds an item to the underlying storage, signaling pertinent notifications. Attempts to enforce homogenous container. */
  /** \brief Moves a container of QObjects to the underlying storage, signaling pertinent notifications. */
  template<template<typename, typename...> class TContainer, typename QObjPtr, typename... Args>
  void appendMany(TContainer<QObjPtr, Args...>&& objects) {
    static_assert(std::is_same<QObjPtr, std::unique_ptr<QObject>>::value, "Invalid parameter type for templated member function QObjectListModel::appendMany. Must be containter_type<std::unique_ptr<QObject>>");
    beginInsertRows(QModelIndex(), rowCount(), rowCount() + objects.size() - 1);
    items_.insert(items_.end(), std::make_move_iterator(objects.begin()), std::make_move_iterator(objects.end()));
    endInsertRows();
  }

private:
  std::vector<std::unique_ptr<QObject>> items_;     /**< \brief Backing storage; owned by model so added items can be properly notified. */
  const std::unique_ptr<const QObject>  prototype_; /**< \brief Derived QObject type used to generate roles from introspected Q_PROPERTYs. */
  QHash<int, QByteArray>                roles_;     /**< \brief Representation of most derived QObject's "columns" used by Qt Views. */
};

#endif // QOBJECTLISTMODEL_HXX
