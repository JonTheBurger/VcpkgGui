#ifndef BACKEND_HXX
#define BACKEND_HXX

#include <memory>

#include <QDebug>
#include <QHash>
#include <QObject>
#include <QPointer>
#include <QProcess>
#include <QSet>
#include <QStringListModel>

#include "AuxFilterModel.hxx"

class QObjectListModel;
/** \brief Provides entrypoint interface for QML to depend upon. */
class Backend : public QObject {
  Q_OBJECT
  Q_DISABLE_COPY(Backend)
  Q_PROPERTY(AuxFilterModel* model READ model NOTIFY modelChanged)
  Q_PROPERTY(QStringListModel* supportedTriplets READ supportedTriplets NOTIFY supportedTripletsChanged)
  Q_PROPERTY(QString triplet READ triplet WRITE setTriplet NOTIFY tripletChanged)
  Q_PROPERTY(QString vcpkgPath READ vcpkgPath WRITE setVcpkgPath NOTIFY vcpkgPathChanged)
  Q_PROPERTY(bool isLoading READ isLoading WRITE setIsLoading NOTIFY isLoadingChanged)

public:
  explicit Backend();
  ~Backend() override;
  Q_INVOKABLE void  loadPackages();                                 /**< \brief Loads vcpkg packages into Model. */
  Q_INVOKABLE void  syncPackages();                                 /**< \brief Installs newly selected pagates, uninstalled deselected packages. */
  Q_INVOKABLE void  setPackageFilter(const QString& filter);        /**< \brief Filters packages by name string. */
  Q_INVOKABLE void  useInstallFilter(bool useFilter);               /**< \brief Filters packages by installed if true, else no filter. */
  Q_INVOKABLE void  updateDirty(const QString& name, bool install); /**< \brief Updates dirty list with the given item. */
  AuxFilterModel*   model();                                        /**< \brief Accessor method used for Q_PROPERTY. */
  QStringListModel* supportedTriplets();                            /**< \brief Accessor method used for Q_PROPERTY. */
  QString           triplet() const;                                /**< \brief Accessor method used for Q_PROPERTY. */
  void              setTriplet(QString triplet);
  QString           vcpkgPath() const;
  void              setVcpkgPath(const QString& path);
  QString           vcpkgExe() const;
  bool              isLoading() const;
  void              setIsLoading(bool isLoading);

signals:
  void modelChanged();             /**< \brief Notification method used for Q_PROPERTY. */
  void supportedTripletsChanged(); /**< \brief Notification method used for Q_PROPERTY. */
  void tripletChanged();           /**< \brief Notification method used for Q_PROPERTY. */
  void vcpkgPathChanged();         /**< \brief Notification method used for Q_PROPERTY. */
  void isLoadingChanged();         /**< \brief Notification method used for Q_PROPERTY. */

private:
  std::unique_ptr<QObjectListModel> modelData_;         /**< \brief vcpkg package info Model and backing store for QML ListView. */
  std::unique_ptr<AuxFilterModel>   model_;             /**< \brief String filtering interface for QML ListView. */
  std::unique_ptr<QStringListModel> supportedTriplets_; /**< \brief CPU/OS/Linkage options available for vcpkg packages. */
  QString                           triplet_;           /**< \brief Selected CPU/OS/Linkage option to install packages with. */
  QString                           vcpkgPath_;
  QSet<QString>                     installedPackages_; /**< \brief installed vcpkg packages for the current triplet. */
  QHash<QString, bool>              dirtyPackages_;
  int                               nameRole_;
  int                               isInstalledRole_;
  bool                              isLoading_;

  void loadDefaultLocationAsync();
  void loadTripletsAsync();
  void vcpkgSearchAsync();
  void vcpkgListAsync();
  void installAsync(const QStringList& newPackages);
  void removeAsync(const QStringList& doomedPackages);
  void parseVcpkgTriplets(const QString& vcpkgOutput); /**< \brief Determines the triplets supported by vcpkg. */
  void parseVcpkgList(const QString& vcpkgOutput);     /**< \brief Determines the installed vcpkg packages for the current triplet. */
  void parseVcpkgSearch(const QString& vcpkgOutput);   /**< \brief Queries for available vcpkg packages. */
  void parseVcpkgInstall(const QString& vcpkgOutput);

  template<typename F1 = void (*)(const QString&), typename F2 = void (*)(void)>
  void runProcess(const QString& pname, const QStringList& args, F1 successHandler = [](const QString&) {}, F2 completeHandler = []() {}) {
    QPointer<QProcess> ps(new QProcess);
    connect(ps,
            static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
            this,
            [this, ps, successHandler, completeHandler](int exitCode, QProcess::ExitStatus exitStatus) {
              QString output = ps->readAllStandardOutput();
              if (exitCode == 0 && exitStatus == QProcess::ExitStatus::NormalExit) {
                successHandler(output);
              } else {
                qDebug() << output << ps->readAllStandardError();
              }
              completeHandler();
              ps->deleteLater();
            });
    ps->start(pname, args);
  }
};

#endif // BACKEND_HXX
