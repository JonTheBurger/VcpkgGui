#ifndef VCPKGITEM_HXX
#define VCPKGITEM_HXX

#include <memory>

#include "DeclQmlProperty.hxx"

/** \brief Qt Model Item representing a vcpkg package. */
class PackageInfo final : public QObject {
  Q_OBJECT
  Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
  Q_PROPERTY(QString version READ version WRITE setVersion NOTIFY versionChanged)
  Q_PROPERTY(QString description READ description WRITE setDescription NOTIFY descriptionChanged)
  Q_PROPERTY(bool isInstalled READ isInstalled WRITE setIsInstalled NOTIFY isInstalledChanged)

public:
  PackageInfo(QString name, QString version, QString description, bool isInstalled, QObject* parent = nullptr)
      : QObject(parent)
      , name_(std::move(name))
      , version_(std::move(version))
      , description_(std::move(description))
      , isInstalled_(isInstalled) {}

  static const constexpr char* const NameRoleStr        = "name";        /**< \brief Role value string used to lookup int role ID for use in String sorting/filtering. */
  static const constexpr char* const IsInstalledRoleStr = "isInstalled"; /**< \brief Role value string used to lookup int role ID for use in Boolean sorting/filtering. */

  static std::unique_ptr<PackageInfo> Default() {
    return std::make_unique<PackageInfo>(QStringLiteral("name"), QStringLiteral("version"), QStringLiteral("description"), false);
  }

  QString name() const { return name_; }
  void    setName(QString name) {
    if (name != name_) {
      name_ = std::move(name);
      nameChanged(name_);
    }
  }
  QString version() const { return version_; }
  void    setVersion(QString version) {
    if (version != version_) {
      version_ = std::move(version);
      versionChanged(version_);
    }
  }
  QString description() const { return description_; }
  void    setDescription(QString description) {
    if (description != description_) {
      description_ = std::move(description);
      descriptionChanged(description_);
    }
  }
  bool isInstalled() const { return isInstalled_; }
  void setIsInstalled(bool isInstalled) {
    if (isInstalled != isInstalled_) {
      isInstalled_ = isInstalled;
      isInstalledChanged(isInstalled_);
    }
  }

signals:
  void nameChanged(QString newName);
  void versionChanged(QString newVersion);
  void descriptionChanged(QString version);
  void isInstalledChanged(bool isInstalled);

private:
  QString name_;
  QString version_;
  QString description_;
  bool    isInstalled_;
};

#endif // VCPKGITEM_HXX
