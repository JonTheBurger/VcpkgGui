#ifndef DECLQMLPROPERTY_HXX
#define DECLQMLPROPERTY_HXX

#include <QObject>

/** \brief Creates Q_PROPERTY member and notification function for provided type/name parameter pairs to a maximum of 5 properties. */
#define DECL_QMLPROPERTIES(...)           \
  DECL_QMLPROPERTY_N(VARG_C(__VA_ARGS__)) \
  (__VA_ARGS__)
/** \copydoc DECL_QMLPROPERTIES */
#define DECL_QMLPROPERTY(Type, name) DECL_QMLPROPERTY2(Type, name)

/** \brief Calls DECL_QMLPROPERTY macro implementation with N parameters. */
#define DECL_QMLPROPERTY_N(N) PP_CONCAT(DECL_QMLPROPERTY, N)
/** \brief concatenate preprocessor tokens A and B without expanding macro definitions. */
#define PP_CONCAT(L, R) L##R
/** \brief Variadic Argument Count returns the number of variadic macro arguments up to 10. */
#define VARG_C(...) VARG_C_LOOK(__VA_ARGS__, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1)
/** \brief Variadic Argument Count Lookup offsets into arg list to return arg count. */
#define VARG_C_LOOK(N1, N2, N3, N4, N5, N6, N7, N8, N9, N10, N, ...) N

/** Invalid instantiation; missing final "name" parameter */
#define DECL_QMLPROPERTY1(...) static_assert(false, "Missing name parameter for 1st property");

/** \brief Creates Q_PROPERTY member and notification function for provided type/name args. */
#define DECL_QMLPROPERTY2(Type, name)                       \
public:                                                     \
  Q_PROPERTY(Type name MEMBER name##_ NOTIFY name##Changed) \
  Q_SIGNAL void name##Changed(const Type& name##Update);    \
                                                            \
private:                                                    \
  Type name##_;

/** \copydoc DECL_QMLPROPERTY1 */
#define DECL_QMLPROPERTY3(...) static_assert(false, "Missing name parameter for 2nd property");

/** \copydoc DECL_QMLPROPERTIES */
#define DECL_QMLPROPERTY4(Type, name, ...) \
  DECL_QMLPROPERTY2(Type, name)            \
  DECL_QMLPROPERTY2(__VA_ARGS__)

/** \copydoc DECL_QMLPROPERTY1 */
#define DECL_QMLPROPERTY5(...) static_assert(false, "Missing name parameter for 3rd property");

/** \copydoc DECL_QMLPROPERTIES */
#define DECL_QMLPROPERTY6(Type, name, ...) \
  DECL_QMLPROPERTY2(Type, name)            \
  DECL_QMLPROPERTY4(__VA_ARGS__)

/** \copydoc DECL_QMLPROPERTY1 */
#define DECL_QMLPROPERTY7(...) static_assert(false, "Missing name parameter for 4th property");

/** \copydoc DECL_QMLPROPERTIES */
#define DECL_QMLPROPERTY8(Type, name, ...) \
  DECL_QMLPROPERTY2(Type, name)            \
  DECL_QMLPROPERTY6(__VA_ARGS__)

/** \copydoc DECL_QMLPROPERTY1 */
#define DECL_QMLPROPERTY9(...) static_assert(false, "Missing name parameter for 5th property");

/** \copydoc DECL_QMLPROPERTIES */
#define DECL_QMLPROPERTY10(Type, name, ...) \
  DECL_QMLPROPERTY2(Type, name)             \
  DECL_QMLPROPERTY8(__VA_ARGS__)

#endif // DECLQMLPROPERTY_HXX
