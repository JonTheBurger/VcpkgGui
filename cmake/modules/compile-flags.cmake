if ("${GLOVE_BUILD_TESTS}" AND "${CMAKE_CXX_COMPILER_ID}" MATCHES "(GNU|Clang|Intel)")
    # add_compile_options doesn't automatically set the coverage flags for the linker
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -ftest-coverage -fprofile-arcs")
endif()

set(IS_GNU_COMPATIBLE
    $<OR:$<CXX_COMPILER_ID:GNU>,$<CXX_COMPILER_ID:Clang>,$<CXX_COMPILER_ID:Intel>>
)

add_compile_options(
    $<$<AND:$<BOOL:${${PROJECT_NAME}_DEV}>,$<BOOL:${IS_GNU_COMPATIBLE}>>:-Werror>
    $<$<AND:$<BOOL:${${PROJECT_NAME}_DEV}>,$<CXX_COMPILER_ID:MSVC>>:/WX>
    $<$<BOOL:${IS_GNU_COMPATIBLE}>:-Wall>
    $<$<BOOL:${IS_GNU_COMPATIBLE}>:-Wextra>
    $<$<BOOL:${IS_GNU_COMPATIBLE}>:-pedantic>
#    $<$<BOOL:${IS_GNU_COMPATIBLE}>:-Wnull-dereference>
    $<$<BOOL:${IS_GNU_COMPATIBLE}>:-Wold-style-cast>
    $<$<BOOL:${IS_GNU_COMPATIBLE}>:-Wdouble-promotion>
    $<$<BOOL:${IS_GNU_COMPATIBLE}>:-Wshadow>
    $<$<BOOL:${IS_GNU_COMPATIBLE}>:-Wformat=2>
    $<$<BOOL:${IS_GNU_COMPATIBLE}>:-Wformat=2>
#    $<$<CXX_COMPILER_ID:GNU>:-Wduplicated-cond>
#    $<$<CXX_COMPILER_ID:GNU>:-Wduplicated-branches>
    $<$<CXX_COMPILER_ID:GNU>:-Wlogical-op>
#    $<$<CXX_COMPILER_ID:GNU>:-Wrestrict>
    $<$<CXX_COMPILER_ID:MSVC>:/W4>
    $<$<CXX_COMPILER_ID:MSVC>:/MP>
)
