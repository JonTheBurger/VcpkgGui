#include <cstdlib>

#include <QDebug>
#include <QGuiApplication>
#include <QIcon>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "Backend.hxx"

int main(int argc, char* argv[]) {
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
  QGuiApplication       app(argc, argv);
  Backend               backend;
  QQmlApplicationEngine engine;
  QQmlContext&          ctx = *engine.rootContext();

  ctx.setContextProperty("backend", &backend);
  Q_INIT_RESOURCE(res);
  QGuiApplication::setWindowIcon(QIcon(QStringLiteral(":/icon.svg")));
  engine.load(QUrl(QStringLiteral("qrc:/Main.qml")));
  if (engine.rootObjects().isEmpty()) {
    return EXIT_FAILURE;
  }
  const auto result = QGuiApplication::exec();
  Q_CLEANUP_RESOURCE(res);
  return result;
}
